#include "Knight.h"
#include "board.h"
Knight::Knight(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'n';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'N';
	}
}

Knight::~Knight()
{
}

string Knight::liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools *(&_board)[SIZE_BOARD][SIZE_BOARD])
{
	// liggle movement for tool:
	// knight
	if (source[0] + 2 == dest[0] && source[1] - 1 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] + 2 == dest[0] && source[1] + 1 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] + 1 == dest[0] && source[1] - 2 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] + 1 == dest[0] && source[1] + 2 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] - 2 == dest[0] && source[1] - 1 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] - 2 == dest[0] && source[1] + 1 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] - 1 == dest[0] && source[1] - 2 == dest[1])
	{
		return RESULT_ZERO;
	}
	else if (source[0] - 1 == dest[0] && source[1] + 2 == dest[1])
	{
		return RESULT_ZERO;
	}
	return RESULT_SIX;
}
