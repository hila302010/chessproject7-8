#include "King.h"
#include "board.h"
King::King(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'k';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'K';
	}
}

King::~King()
{
}
string King::liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools *(&_board)[SIZE_BOARD][SIZE_BOARD])
{
	// liggle movement for tool:

	if (source[1] == dest[1] && source[0] - 1 == dest[0]) // valid move up
		return RESULT_ZERO;

	if (source[1] == dest[1] && source[0] + 1 == dest[0]) // valid move down
		return RESULT_ZERO;

	if (source[0] == dest[0] && source[1] - 1 == dest[1]) // valid move left
		return RESULT_ZERO;

	if (source[0] == dest[0] && source[1] + 1 == dest[1]) // valid move right
		return RESULT_ZERO;

	if (source[0] - 1 == dest[0] && source[1] - 1 == dest[1]) // valid move up left corner
		return RESULT_ZERO;

	if (source[0] - 1 == dest[0] && source[1] + 1 == dest[1]) // valid move up right corner
		return RESULT_ZERO;

	if (source[0] + 1 == dest[0] && source[1] - 1 == dest[1]) // valid move down left corner
		return RESULT_ZERO;

	if (source[0] + 1 == dest[0] && source[1] + 1 == dest[1]) // valid move down right corner
		return RESULT_ZERO;

	return RESULT_SIX;
}
