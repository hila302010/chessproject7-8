#include "board.h"
#pragma once

// constractur for the board.
board::board()
{
	// white player starts
	turn = WHITE;

	King* bKing = new King(BLACK);
	King* wKing = new King(WHITE);

	Rook* bRookRight = new Rook(BLACK);
	Rook* bRookLeft = new Rook(BLACK);

	Rook* wRookRight = new Rook(WHITE);
	Rook* wRookLeft = new Rook(WHITE);

	Knight* bKnightRight = new Knight(BLACK);
	Knight* bKnightLeft = new Knight(BLACK);

	Knight* wKnightRight = new Knight(WHITE);
	Knight* wKnightLeft = new Knight(WHITE);

	Pawn* bPawn = new Pawn(BLACK);
	Pawn* wPawn = new Pawn(WHITE);

	Bishop* bBishopRight = new Bishop(BLACK);
	Bishop* bBishopLeft = new Bishop(BLACK);

	Bishop* wBishopRight = new Bishop(WHITE);
	Bishop* wBishopLeft = new Bishop(WHITE);

	Queen* bQueen = new Queen(BLACK);
	Queen* wQueen = new Queen(WHITE);

	// restart the all board as empty (#)
	for (int i = 0; i < SIZE_BOARD; i++) 
	{
		for (int j = 0; j < SIZE_BOARD; j++)
		{
			Empty* empty = new Empty(EMPTY);
			_board[i][j] = empty;
		}
	}

	// set black pawns at raw 1 (second raw)
	for (int i = 0; i < SIZE_BOARD; i++) 
	{
		_board[SECOND_RAW][i] = bPawn;
	}

	// set white pawns at raw 6 (seventh raw) 
	for (int i = 0; i < SIZE_BOARD; i++) 
	{
		_board[SEVEN_RAW][i] = wPawn;
	}

	//init black rooks
	_board[0][0] = bRookLeft;
	_board[0][7] = bRookRight;
	// init white rooks
	_board[7][0] = wRookLeft;
	_board[7][7] = wRookRight;
	// init black&white kings
	_board[0][4] = bKing;
	_board[7][4] = wKing;
	//init black knights
	_board[0][1] = bKnightLeft;
	_board[0][6] = bKnightRight;
	//init white knights
	_board[7][1] = wKnightLeft;
	_board[7][6] = wKnightRight;
	//init black bishops
	_board[0][2] = bBishopLeft;
	_board[0][5] = bBishopRight;
	//init white bishops
	_board[7][2] = wBishopLeft;
	_board[7][5] = wBishopRight;
	// init black&white queens
	_board[0][3] = bQueen;
	_board[7][3] = wQueen;

	//set the locations of king (for chess).
	_bKingLocation[0] = 0;
	_bKingLocation[1] = 4;
	_wKingLocation[0] = 7;
	_wKingLocation[1] = 4;

}

board::~board()
{
	for (int i = 0; i < SIZE_BOARD; i++)
	{
		for (int j = 0; j < SIZE_BOARD; j++)
		{
			delete _board[i][j];
		}
	}
}

// function prints the board
void board::printBoard()
{
	for (int i = 0; i < SIZE_BOARD; i++)
	{
		for (int j = 0; j < SIZE_BOARD; j++)
		{
			cout << _board[i][j]->getTool() << " ";
		}
		cout << endl;
	}
}

int board::getTurn()
{
	return this->turn;
}

void board::setTurn(int turn)
{
	this->turn = turn;
}

void board::setSource(string msgFromFronted)
{
	_source[0] = 7 - (msgFromFronted[1] - 49);
	_source[1] = (msgFromFronted[0] - 97);
}

void board::setDest(string msgFromFronted)
{
	_dest[0] = 7 - (msgFromFronted[3] - 49);
	_dest[1] = (msgFromFronted[2] - 97);
}

// move the du meimadi board to string - to send str msg(board) to fronted
void board::moveBoardToString(char(&stringBoard)[CHAR_BOARD_SIZE])
{
	int index = 0;
	for (int i = 0; i < SIZE_BOARD; i++) {
		for (int j = 0; j < SIZE_BOARD; j++)
		{
			stringBoard[index] = _board[i][j]->getTool();
			index++;
		}
	}

	stringBoard[CHAR_BOARD_SIZE - 2] = turn + '0'; // put the turn.
	stringBoard[CHAR_BOARD_SIZE - 1] = NULL;
}

//The function check if the player can move.
string board::checkMove(string msg)
{
	tools* toolAtSource;
	tools* toolAtDest;
	Empty* empty = new Empty(EMPTY);
	string result;
	setSource(msg);
	setDest(msg);
	toolAtSource = _board[_source[0]][_source[1]];
	toolAtDest = _board[_dest[0]][_dest[1]];
	cout << "tool src " << toolAtSource->getTool() << " tool color " << toolAtSource->getColor() << endl;
	cout << " dst color " << toolAtDest->getTool() << " dst color " << toolAtDest->getColor() << endl;
	// check if tool at source can move
	result = checkValid(toolAtSource); 
	if (result == RESULT_ZERO)
	{
		_board[_dest[0]][_dest[1]] = toolAtSource;
		_board[_source[0]][_source[1]] = empty;
	}
	delete empty;
	return result;
}

string board::checkValid(tools * toolAtSource)
{
	// liggle movement for tool:
	if (toolAtSource->getColor() == getTurn()) // source tool is player's tool
	{
		if (_board[_dest[0]][_dest[1]]->getColor() != getTurn()) // dest tool is empty or second player
		{
			if (toolAtSource->liggle(_source, _dest, _board) == RESULT_ZERO) // valid move
			{
				if (toolAtSource->getTool() == 'k' || toolAtSource->getTool() == 'K') { //result 4
					changeKingLocation(toolAtSource->getColor());
				}
				return RESULT_ZERO;
			}
			else {
				return  toolAtSource->liggle(_source, _dest, _board);
			}
		}
		else {
			return RESULT_THREE;
		}
	}
	else {
		return RESULT_TWO;
	}
}

void board::changeKingLocation(int kingColor)
{
	// change black king
	if (kingColor == BLACK)
	{
		_bKingLocation[0] = _dest[0];
		_bKingLocation[1] = _dest[1];
	}
	// change white king
	else
	{
		_wKingLocation[0] = _dest[0];
		_wKingLocation[1] = _dest[1];
	}
}

bool board::checkCheck(int(&kingLocation)[SIZE_LOCATION], int color)
{
	return false;
}

