#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include "board.h"

Queen::Queen(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'q';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'Q';
	}
}

Queen::~Queen()
{
}

string Queen::liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools *(&_board)[SIZE_BOARD][SIZE_BOARD])
{
	// liggle movement for tool:
	Rook rook;
	Bishop bishop;
	// queen
	if (rook.liggle(source, dest, _board) == RESULT_ZERO || bishop.liggle(source, dest, _board) == RESULT_ZERO)
		return RESULT_ZERO;
	return RESULT_SIX;
}
