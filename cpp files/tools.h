#pragma once
#include <iostream>
using namespace std;
//Defining results to throw to fronted (protocol):
#define	RESULT_ZERO "0" // 0 - valid move.
#define	RESULT_THREE "3" // 3 - invalid move (destination is not free).
#define	RESULT_SIX "6" // 6 - invalid move (illegal movement with piece).

//Defining sizes for arrays:
#define SIZE_BOARD 8
#define SIZE_LOCATION 2

//Defining colors to :
#define WHITE 0
#define BLACK 1
#define EMPTY 2

#define SEVEN_RAW 6
#define SECOND_RAW 1
#define FIVE_RAW 4
#define FOURTH_RAW 3

class tools
{
protected:
	int _color; // black or white or empty 
	char _tool;
public:
	//constructor
	tools();
	//Destructor:
	virtual ~tools();

	//Get thing for some piece:
	int getColor();
	char getTool();

	//Moving:
	virtual string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&_board)[SIZE_BOARD][SIZE_BOARD]) = 0;

};