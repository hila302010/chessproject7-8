#include "Rook.h"
#include "board.h"

Rook::Rook()
{
}

Rook::Rook(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'r';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'R';
	}
}

string Rook::liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools *(&_board)[SIZE_BOARD][SIZE_BOARD])
{
	// liggle movement for tool:
	// rook
	if (source[1] == dest[1]) // up down
	{
		// move down
		if (source[0] < dest[0]) {
			for (int i = source[0] + 1; i < dest[0]; i++)
				if (_board[i][source[1]]->getColor() != EMPTY)
					return RESULT_FIVE;
		}
		// move up
		else if (source[0] > dest[0])
		{
			for (int i = source[0] - 1; i > dest[0]; i--)
				if (_board[i][source[1]]->getColor() != EMPTY)
					return RESULT_FIVE;
		}
		return RESULT_ZERO;
	}
	else if (source[0] == dest[0]) // right left
	{
		// move right
		if (source[1] < dest[1]) {
			for (int i = source[1] + 1; i < dest[1]; i++)
				if (_board[source[0]][i]->getColor() != EMPTY)
					return RESULT_FIVE;
		}
		// move left
		else if (source[1] > dest[1])
		{
			for (int i = source[1] - 1; i > dest[1]; i--)
				if (_board[source[0]][i]->getColor() != EMPTY)
					return RESULT_FIVE;
		}
		return RESULT_ZERO;
	}
	return RESULT_SIX;
}

Rook::~Rook()
{
}