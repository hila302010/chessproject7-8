#include "Pawn.h"
#include "board.h"

Pawn::Pawn(int color)
{
	if (color == BLACK) {
		this->_color = BLACK;
		this->_tool = 'p';
	}
	else {
		this->_color = WHITE;
		this->_tool = 'P';
	}
}

Pawn::~Pawn()
{
}

string Pawn::liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools *(&_board)[SIZE_BOARD][SIZE_BOARD])
{
	// BONUS En-passent:
	// liggle movement for tool:
	if (_board[dest[0]][dest[1]]->getColor() == EMPTY)
	{
		// white pawn
		if (_board[source[0]][source[1]]->getColor() == WHITE)
		{
			if ((source[1] == dest[1] && source[0] - 1 == dest[0]) || (source[1] == dest[1] && source[0] - 2 == dest[0] && source[0] == SEVEN_RAW)) // valid move
				return RESULT_ZERO;
		}
		// black pawn
		if (_board[source[0]][source[1]]->getColor() == BLACK)
		{
			if ((source[1] == dest[1] && source[0] + 1 == dest[0]) || (source[1] == dest[1] && source[0] + 2 == dest[0] && source[0] == SECOND_RAW)) // valid move
				return RESULT_ZERO;
		}
	}
	else // if pawn is gonn'a eat
	{
		// white pawn
		if (_board[source[0]][source[1]]->getColor() == WHITE)
		{
			if ((source[0] - 1 == dest[0] && source[1] - 1 == dest[1])) // valid move left
				return RESULT_ZERO;

			if ((source[0] - 1 == dest[0] && source[1] + 1 == dest[1])) // valid move right
				return RESULT_ZERO;
		}
		// black pawn
		if (_board[source[0]][source[1]]->getColor() == BLACK)
		{
			if ((source[0] + 1 == dest[0] && source[1] - 1 == dest[1])) // valid move left
				return RESULT_ZERO;

			if ((source[0] + 1 == dest[0] && source[1] + 1 == dest[1])) // valid move right
				return RESULT_ZERO;
		}
	}

	return RESULT_SIX;
}


