#include "Pipe.h"
#include <iostream>
#include <thread>
#include <string>
#include "board.h"
using namespace std;
void main()
{
	srand(time_t(NULL));

	Pipe p;
	board* gameBoard = new board();
	char stringBoard[CHAR_BOARD_SIZE]; // board as char* to send to the server
	string moving = "0";
	string result;
	char resultChar[2];
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}
	// my code:

	char msgToGraphics[BUFFER_SIZE];
	// msgToGraphics should contain the board string accord the protocol
	gameBoard->moveBoardToString(stringBoard);
	
	strcpy_s(msgToGraphics, stringBoard);
	p.sendMessageToGraphics(msgToGraphics);   // send the string board

    // get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		gameBoard->printBoard();

		//convert to char array:
		result = gameBoard->checkMove(msgFromGraphics);
		resultChar[0] = result[0];
		resultChar[1] = NULL;
		// change the turn
		if (result == RESULT_ZERO || result == RESULT_ONE) 
		{
			if (gameBoard->getTurn() == WHITE)
			{
				gameBoard->setTurn(BLACK);
			}
			else
			{
				gameBoard->setTurn(WHITE);
			}
		}

		strcpy_s(msgToGraphics, resultChar); // msgToGraphics should contain the result of the operation
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}