#pragma once
#include "tools.h"
#include "EmptySquare.h"

// This class is the empty squares in the board
class Queen : public tools
{
public:
	//ctor
	Queen(int color);
	//dtor
	~Queen();
	// moving function
	virtual string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&_board)[SIZE_BOARD][SIZE_BOARD]);

};
