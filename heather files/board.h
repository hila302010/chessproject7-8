#pragma once
#include <iostream>
#include "tools.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Pawn.h"
#include "Pipe.h"
#include "Queen.h"
#include "Rook.h"

//Defining results to throw to fronted (protocol):
#define	RESULT_ONE "1" // 1 - valid move (you made chess).
#define	RESULT_TWO "2" // 2 - invalid move (not your player).
#define	RESULT_FOUR "4" // 4 - invalid move (chess on yourself will occur).
#define	RESULT_FIVE "5" // 5 - invalid move (out of bounds).
#define	RESULT_SEVEN "7" // 7 - Invalid move - source and dest are equal.

#define CHAR_BOARD_SIZE 66
//Defining colors to pieces:
#define WHITE 0
#define BLACK 1
#define EMPTY 2
#define SIZE_OF_KING_LOCATION 2

class board
{
private:
	tools * _board[SIZE_BOARD][SIZE_BOARD];
	int turn; // white first

	int _wKingLocation[SIZE_OF_KING_LOCATION]; // white king - 
	int _bKingLocation[SIZE_OF_KING_LOCATION]; // black king - 

	int _source[SIZE_LOCATION];
	int _dest[SIZE_LOCATION];

public:
	// constructor
	board();
	~board();

	//Printing:
	void printBoard();

	//Get and set things in board:
	int getTurn();
	void setTurn(int turn);
	void setSource(string msgFromFronted);
	void setDest(string msgFromFronted);

	void moveBoardToString(char(&stringBoard)[CHAR_BOARD_SIZE]); // string board with 66 chars
	string checkMove(string msg); // no tool on the way
	// if the tool can make the move
	// didn't make chess on yourself
	string checkValid(tools * toolAtSource);
	void changeKingLocation(int kingColor);
	bool checkCheck(int(&kingLocation)[SIZE_LOCATION], int color);

};