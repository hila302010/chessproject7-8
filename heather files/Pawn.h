#pragma once
#include "tools.h"
#include "EmptySquare.h"

// This class is the pawns in the board
class Pawn : public tools
{
public:
	//ctor
	Pawn(int color);
	//dtor
	~Pawn();
	string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&_board)[SIZE_BOARD][SIZE_BOARD]);
	// moving function

};
