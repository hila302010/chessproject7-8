#pragma once
#include "tools.h"
#include "EmptySquare.h"

// This class is the empty squares in the board
class Bishop : public tools
{
public:
	//ctor
	Bishop();
	Bishop(int color);
	//dtor
	~Bishop();
	// moving function
	virtual string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&_board)[SIZE_BOARD][SIZE_BOARD]);

};
