#pragma once
#include "tools.h"
#include "EmptySquare.h"

// This class is the empty squares in the board
class Knight : public tools
{
public:
	//ctor
	Knight(int color);
	//dtor
	~Knight();
	// moving function
	virtual string liggle(int source[SIZE_LOCATION], int dest[SIZE_LOCATION], tools* (&_board)[SIZE_BOARD][SIZE_BOARD]);

};

